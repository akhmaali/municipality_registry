package com.municipality.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MunicipalityRegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MunicipalityRegistryApplication.class, args);
    }

}
