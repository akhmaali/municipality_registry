package com.municipality.demo.services;

import com.municipality.demo.dto.TownDto;
import com.municipality.demo.models.Town;
import com.municipality.demo.repository.TownRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Service for [Town]s
 */
@Service
public class TownService {

    /**
     * Zip file URL
     */
    private static final String ZIP_URL = "https://vdp.cuzk.cz/vymenny_format/soucasna/20201231_OB_573060_UZSZ.xml.zip";

    /**
     * Path for zip file
     */
    private static final String ZIP_FILE_PATH = "data/data.zip";

    /**
     * XML element name for town
     */
    private static final String TOWN_ELEMENT_NAME = "vf:Obec";

    /**
     * XML element name for town code
     */
    private static final String TOWN_CODE_ELEMENT_NAME = "obi:Kod";

    /**
     * XML element name for town name
     */
    private static final String TOWN_NAME_ELEMENT_NAME = "obi:Nazev";

    /**
     * Repository for [Town]s
     */
    private final TownRepository townRepository;

    /**
     * Service for [TownPart]s
     */
    private final TownPartService townPartService;

    /**
     * Constructor for [TownService]
     *
     * @param townRepository  [TownRepository] repository
     * @param townPartService [TownPartService] service
     */
    @Autowired
    public TownService(TownRepository townRepository, TownPartService townPartService) {
        this.townRepository = townRepository;
        this.townPartService = townPartService;
    }

    /**
     * Get [Town] with the given code
     *
     * @param code town code
     * @return found [Town] in DB
     */
    @Transactional
    public Town findByCode(Long code) {
        return townRepository.getOne(code);
    }

    /**
     * Get all [Town]s
     *
     * @return list of [Town]s
     */
    @Transactional
    public List<Town> findAll() {
        return townRepository.findAll();
    }

    /**
     * Persist new [Town]
     *
     * @param town [Town] to be persisted
     */
    @Transactional
    public void saveTown(Town town) {
        townRepository.save(town);
    }

    /**
     * Persist list of [Town]s
     *
     * @param towns list of [Town]s to be persisted
     */
    @Transactional
    public void saveAll(List<Town> towns) {
        townRepository.saveAll(towns);
    }

    /**
     * Delete [Town] with the given code
     *
     * @param code town code
     */
    @Transactional
    public void deleteByCode(Long code) {
        townRepository.deleteById(code);
    }

    /**
     * Get [Town] in the form of [TownDto]
     *
     * @param code town code
     * @return [TownDto]
     */
    public TownDto getTownByCode(Long code) {
        return getDto(findByCode(code));
    }

    /**
     * Get all [Town]s in the form of [TownDto]s
     *
     * @return collection of [TownDto]s
     */
    public Collection<TownDto> getAllTowns() {
        return findAll().stream().map(this::getDto).collect(Collectors.toList());
    }

    /**
     * Create new [Town] from the given [TownDto]
     *
     * @param townDto [TownDto]
     */
    public void createTown(TownDto townDto) {
        saveTown(new Town(
                townDto.getCode(),
                townDto.getName(),
                townDto.getTownParts().stream().map(townPartService::getEntity).collect(Collectors.toList()))
        );
    }

    /**
     * Update [Town] with the given code based on the given [TownDto]
     *
     * @param code    town code
     * @param townDto [TownDto]
     */
    public void updateTown(Long code, TownDto townDto) {
        Town town = findByCode(code);
        town.setName(townDto.getName());
        town.setTownParts(townDto.getTownParts().stream().map(townPartService::getEntity).collect(Collectors.toList()));
        saveTown(town);
    }

    /**
     * Get Dto for the given [Town]
     *
     * @param town [Town]
     * @return [TownDto] for the given [Town]
     */
    public TownDto getDto(Town town) {
        return new TownDto(
                town.getCode(),
                town.getName(),
                town.getTownParts().stream().map(townPartService::getDto).collect(Collectors.toList())
        );
    }

    /**
     * Migrates XML data from the URL
     * Downloads zip file from the URL, iterates over the entries inside the zip, parses and saves the contents
     */
    public void migrateXmlData() {
        try {
            //Download zip file
            downloadZip(ZIP_URL, ZIP_FILE_PATH);

            //Extract, parse and save xml data from zip
            ZipFile zipFile = new ZipFile(ZIP_FILE_PATH);

            for (Enumeration<? extends ZipEntry> entries = zipFile.entries(); entries.hasMoreElements(); ) {
                ZipEntry zipEntry = entries.nextElement();
                InputStream is = zipFile.getInputStream(zipEntry);
                Document document = buildDocument(is);
                saveAll(parseTowns(document));
                townPartService.parseAndSaveTownParts(document);
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
    }

    /**
     * Downloads zip file from the URL
     *
     * @param zipURL      url of the zip file
     * @param zipFilePath file path for the zip
     */
    public void downloadZip(String zipURL, String zipFilePath) {
        try {
            URL url = new URL(zipURL);
            ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(zipFilePath);
            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates document object for the given [InputStream]
     *
     * @param is [InputStream]
     * @return [Document] object
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public Document buildDocument(InputStream is) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse(is);
        document.getDocumentElement().normalize();
        return document;
    }

    /**
     * Parses towns from the XML [Document]
     *
     * @param document XML [Document]
     * @return list of parsed [Town]s from the XML
     */
    public List<Town> parseTowns(Document document) {
        NodeList towns = document.getElementsByTagName(TOWN_ELEMENT_NAME);
        List<Town> persistingTowns = new ArrayList<>();

        for (int i = 0; i < towns.getLength(); i++) {
            Node town = towns.item(i);

            if (town.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) town;
                String townCode = element.getElementsByTagName(TOWN_CODE_ELEMENT_NAME).item(0).getTextContent();
                String townName = element.getElementsByTagName(TOWN_NAME_ELEMENT_NAME).item(0).getTextContent();
                persistingTowns.add(new Town(Long.parseLong(townCode), townName));
            }
        }

        return persistingTowns;
    }

}
