package com.municipality.demo.services;

import com.municipality.demo.dto.TownPartDto;
import com.municipality.demo.models.TownPart;
import com.municipality.demo.repository.TownPartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TownPartService {

    /**
     * XML element name for town part
     */
    private static final String TOWN_PART_ELEMENT = "vf:CastObce";

    /**
     * XML element name for town part code
     */
    private static final String TOWN_PART_CODE_ELEMENT = "coi:Kod";

    /**
     * XML element name for town part name
     */
    private static final String TOWN_PART_NAME_ELEMENT = "coi:Nazev";

    /**
     * XML element name for referenced town
     */
    private static final String REFERENCED_TOWN_ELEMENT = "coi:Obec";

    /**
     * XML element name for referenced town code
     */
    private static final String REFERENCED_TOWN_CODE_ELEMENT = "obi:Kod";

    /**
     * Repository for [TownPart]s
     */
    private final TownPartRepository townPartRepository;

    /**
     * Service for [Town]s
     */
    private final TownService townService;

    /**
     * Constructor for [TownPartService]
     *
     * @param townPartRepository [TownPartRepository] repository
     * @param townService        [TownService] service
     */
    @Autowired
    public TownPartService(TownPartRepository townPartRepository, @Lazy TownService townService) {
        this.townPartRepository = townPartRepository;
        this.townService = townService;
    }

    /**
     * Get [TownPart] with the given code
     *
     * @param code town part code
     * @return found [TownPart] in DB
     */
    @Transactional
    public TownPart findByCode(Long code) {
        return townPartRepository.getOne(code);
    }

    /**
     * Get all [TownPart]s
     *
     * @return list of [TownPart]s
     */
    @Transactional
    public List<TownPart> findAll() {
        return townPartRepository.findAll();
    }

    /**
     * Persist new [TownPart]
     *
     * @param townPart [TownPart] to be persisted
     */
    @Transactional
    public void saveTownPart(TownPart townPart) {
        townPartRepository.save(townPart);
    }

    /**
     * Persist list of [TownPart]s
     *
     * @param townParts list of [TownPart]s to be persisted
     */
    @Transactional
    public void saveAll(List<TownPart> townParts) {
        townPartRepository.saveAll(townParts);
    }

    /**
     * Delete [TownPart] with the given code
     *
     * @param code town part code
     */
    @Transactional
    public void deleteByCode(Long code) {
        townPartRepository.deleteById(code);
    }

    /**
     * Get [TownPart] in the form of [TownPartDto]
     *
     * @param code town part code
     * @return [TownPartDto]
     */
    public TownPartDto getTownPartByCode(Long code) {
        return getDto(findByCode(code));
    }

    /**
     * Get all [TownPart]s in the form of [TownPartDto]s
     *
     * @return collection of [TownPartDto]s
     */
    public Collection<TownPartDto> getAllTownParts() {
        return findAll().stream().map(this::getDto).collect(Collectors.toList());
    }

    /**
     * Create new [TownPart] from the given [TownPartDto]
     *
     * @param townPartDto [TownPartDto]
     */
    public void createTownPart(TownPartDto townPartDto) {
        saveTownPart(new TownPart(
                townPartDto.getCode(),
                townPartDto.getName(),
                townService.findByCode(townPartDto.getTownId()))
        );
    }

    /**
     * Update [TownPart] with the given code based on the given [TownPartDto]
     *
     * @param code        town part code
     * @param townPartDto [TownPartDto]
     */
    public void updateTownPart(Long code, TownPartDto townPartDto) {
        TownPart townPart = findByCode(code);
        townPart.setName(townPartDto.getName());
        townPart.setTown(townService.findByCode(townPartDto.getTownId()));
        saveTownPart(townPart);
    }

    /**
     * Get [TownPart] entity from the given [TownPartDto]
     *
     * @param townPartDto [TownPartDto]
     * @return [TownPart] entity object
     */
    public TownPart getEntity(TownPartDto townPartDto) {
        return findByCode(townPartDto.getCode());
    }

    /**
     * Get Dto for the given [TownPart]
     *
     * @param townPart [TownPart]
     * @return [TownPartDto] for the given [TownPart]
     */
    public TownPartDto getDto(TownPart townPart) {
        return new TownPartDto(
                townPart.getCode(),
                townPart.getName(),
                townPart.getTown().getCode()
        );
    }

    /**
     * Parse and save town parts data from the given XML document
     *
     * @param document given [Document]
     */
    public void parseAndSaveTownParts(Document document) {
        NodeList townParts = document.getElementsByTagName(TOWN_PART_ELEMENT);
        List<TownPart> persistingTownParts = new ArrayList<>();

        for (int i = 0; i < townParts.getLength(); i++) {
            Node townPart = townParts.item(i);

            if (townPart.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) townPart;
                String townPartCode = element.getElementsByTagName(TOWN_PART_CODE_ELEMENT).item(0).getTextContent();
                String townPartName = element.getElementsByTagName(TOWN_PART_NAME_ELEMENT).item(0).getTextContent();
                String referencedTownCode = extractReferencedTownCode(element);

                persistingTownParts.add(new TownPart(
                        Long.parseLong(townPartCode),
                        townPartName,
                        townService.findByCode(Long.parseLong(referencedTownCode))
                ));
            }
        }

        saveAll(persistingTownParts);
    }

    /**
     * Extract the referenced town code from the given [Element]
     *
     * @param element [Element]
     * @return referenced town code
     */
    public String extractReferencedTownCode(Element element) {
        NodeList referencedTowns = element.getElementsByTagName(REFERENCED_TOWN_ELEMENT);
        Element referencedTownElement = (Element) referencedTowns.item(0);
        return referencedTownElement.getElementsByTagName(REFERENCED_TOWN_CODE_ELEMENT)
                .item(0).getTextContent();
    }

}
