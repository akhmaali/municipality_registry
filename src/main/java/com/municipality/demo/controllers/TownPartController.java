package com.municipality.demo.controllers;

import com.municipality.demo.dto.TownPartDto;
import com.municipality.demo.services.TownPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Rest controller for [TownPart]s
 */
@RestController
@RequestMapping("/town-parts")
public class TownPartController {

    /**
     * Service for [TownPart]s
     */
    private final TownPartService townPartService;

    /**
     * Constructor for [TownPartController]
     *
     * @param townPartService injecting [TownPartService] service
     */
    @Autowired
    public TownPartController(TownPartService townPartService) {
        this.townPartService = townPartService;
    }

    /**
     * Get all [TownPart]s in DB in the form of [TownPartDto]
     *
     * @return collection of [TownPartDto]s
     */
    @GetMapping("")
    public Collection<TownPartDto> getAllTownParts() {
        return townPartService.getAllTownParts();
    }

    /**
     * Get [TownPart] with the given id (code) in the form of [TownPartDto]
     *
     * @param id town part code
     * @return found [TownPartDto]
     */
    @GetMapping("/{id}")
    public TownPartDto getTownPartByCode(@PathVariable Long id) {
        return townPartService.getTownPartByCode(id);
    }

    /**
     * Creates new [TownPart] from the given [TownPartDto]
     *
     * @param townPartDto [TownPartDto] from which new [TownPart] is created
     */
    @PostMapping("")
    public void createTownPart(@RequestBody TownPartDto townPartDto) {
        townPartService.createTownPart(townPartDto);
    }

    /**
     * Update [TownPart] with the given id (code) and alter based on the given [TownPartDto]
     *
     * @param id          town part code of [TownPart] to update
     * @param townPartDto contents of updated [TownPart]
     */
    @PutMapping("/{id}")
    public void updateTownPart(@PathVariable Long id, @RequestBody TownPartDto townPartDto) {
        townPartService.updateTownPart(id, townPartDto);
    }

    /**
     * Delete [TownPart] with the given id (code)
     *
     * @param id code of [TownPart] to delete
     */
    @DeleteMapping("/{id}")
    public void deleteTownPart(@PathVariable Long id) {
        townPartService.deleteByCode(id);
    }

}
