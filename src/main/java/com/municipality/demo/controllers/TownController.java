package com.municipality.demo.controllers;

import com.municipality.demo.dto.TownDto;
import com.municipality.demo.services.TownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Rest controller for [Town]s
 */
@RestController
@RequestMapping("/towns")
public class TownController {

    /**
     * Service for [Town]s
     */
    private final TownService townService;

    /**
     * Constructor for [TownController]
     *
     * @param townService injecting [TownService] service
     */
    @Autowired
    public TownController(TownService townService) {
        this.townService = townService;
    }

    /**
     * Get all [Town]s in DB in the form of [TownDto]
     *
     * @return collection of [TownDto]s
     */
    @GetMapping("")
    public Collection<TownDto> getAllTowns() {
        return townService.getAllTowns();
    }

    /**
     * Get [Town] with the given id (code) in the form of [TownDto]
     *
     * @param id town code
     * @return found [TownDto]
     */
    @GetMapping("/{id}")
    public TownDto getTownByCode(@PathVariable Long id) {
        return townService.getTownByCode(id);
    }

    /**
     * Starting endpoint for migrating xml data from server to DB
     */
    @PostMapping("/migrate")
    @ResponseStatus(HttpStatus.OK)
    public void migrateXmlData() {
        townService.migrateXmlData();
    }

    /**
     * Creates new [Town] from the given [TownDto]
     *
     * @param townDto [TownDto] from which new [Town] is created
     */
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTown(@RequestBody TownDto townDto) {
        townService.createTown(townDto);
    }

    /**
     * Update [Town] with the given id (code) and alter based on the given [TownDto]
     *
     * @param id      town code of [Town] to update
     * @param townDto contents of updated [Town]
     */
    @PutMapping("/{id}")
    public void updateTown(@PathVariable Long id, @RequestBody TownDto townDto) {
        townService.updateTown(id, townDto);
    }

    /**
     * Delete [Town] with the given id (code)
     *
     * @param id code of [Town] to delete
     */
    @DeleteMapping("/{id}")
    public void deleteTown(@PathVariable Long id) {
        townService.deleteByCode(id);
    }

}
