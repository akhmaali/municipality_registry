package com.municipality.demo.dto;

import lombok.Data;

@Data
public class TownPartDto {

    private Long code;

    private String name;

    private Long townId;

    public TownPartDto(Long code, String name, Long townId) {
        this.code = code;
        this.name = name;
        this.townId = townId;
    }

}
