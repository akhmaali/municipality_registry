package com.municipality.demo.dto;

import lombok.Data;

import java.util.Collection;

@Data
public class TownDto {

    private Long code;

    private String name;

    private Collection<TownPartDto> townParts;

    public TownDto(Long code, String name, Collection<TownPartDto> townParts) {
        this.code = code;
        this.name = name;
        this.townParts = townParts;
    }

}
