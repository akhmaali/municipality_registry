package com.municipality.demo.repository;

import com.municipality.demo.models.Town;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for [Town]
 */
@Repository
public interface TownRepository extends JpaRepository<Town, Long> {

}
