package com.municipality.demo.repository;

import com.municipality.demo.models.TownPart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for [TownPart]
 */
@Repository
public interface TownPartRepository extends JpaRepository<TownPart, Long> {

}
