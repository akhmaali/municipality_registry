package com.municipality.demo.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@Table(name = "towns")
public class Town {

    @Id
    @Column(name = "code")
    private Long code;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "town")
    private Collection<TownPart> townParts;

    public Town() {
    }

    public Town(Long code, String name) {
        this.code = code;
        this.name = name;
    }

    public Town(Long code, String name, Collection<TownPart> townParts) {
        this.code = code;
        this.name = name;
        this.townParts = townParts;
    }

}
