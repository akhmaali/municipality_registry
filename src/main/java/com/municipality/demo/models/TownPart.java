package com.municipality.demo.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "town_parts")
public class TownPart {

    @Id
    private Long code;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "town_id", nullable = false)
    private Town town;

    public TownPart() {
    }

    public TownPart(Long code, String name, Town town) {
        this.code = code;
        this.name = name;
        this.town = town;
    }

}
