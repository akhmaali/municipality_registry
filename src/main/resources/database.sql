-- Table: towns
CREATE TABLE towns (
    code INT NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

-- Table: town_parts
CREATE TABLE town_parts (
    code INT NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    town_id INT NOT NULL,
    FOREIGN KEY (town_id) REFERENCES towns(code)
);

