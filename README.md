# Municipality Registry 
The goal of the project was to fetch data from remote XML file located at the given URL and save parsed data about towns and town parts to database.

## Technologies 
The project is made up from the following:
* JDK 1.11
* Spring Boot 2.4.3
* PostgreSQL

## Features 
* Fetch data from remote XML file and save to database
* CRUD operations for towns and town parts

## Build Database Schema
There is `database.sql` script located at `src\main\resources\database.sql`, which must be run to create the database schema

## Running the application locally
The application runs on port `8080` (context path: `http://localhost:8080/trixi/api/`). There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method in the MunicipalityRegistryApplication class from your IDE.

Alternatively you can use the Spring Boot Maven plugin like so (run from root directory):
```bash
mvn spring-boot:run
```

There is prepared endpoint located at `/trixi/api/towns/migrate`. Make a POST Request to this endpoint and XML data will be transfered to database from the given URL. After that you can integrate with the entities using CRUD operations. There are prepared enpoints for each CRUD operation for `Town` and `TownPart` entities 

## API Description
More detailed in javadoc <br />
`Town`:
* POST `/trixi/api/towns/migrate` migrates XML data about town and town parts to DB
* GET `/trixi/api/towns` gets all towns
* GET `/trixi/api/towns/{id}` gets town with the given id
* POST `/trixi/api/towns` creates new town 
* PUT `/trixi/api/towns/{id}` updates town with the given id
* DELETE `/trixi/api/towns/{id}` deletes town with the given id

`TownPart`:
* GET `/trixi/api/town-parts` gets all town parts
* GET `/trixi/api/town-parts/{id}` gets town part with the given id
* POST `/trixi/api/town-parts` creates new town part
* PUT `/trixi/api/town-parts/{id}` updates town part with the given id
* DELETE `/trixi/api/town-parts/{id}` deletes town part with the given id